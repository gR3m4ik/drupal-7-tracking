(function ($) {

    Drupal.behaviors.TrackingBehavior = {
        attach: function (context, settings) {

            Drupal.ajax['view_tracking'].options.success =  function (response, status) {

                Drupal.ajax.prototype.success.call(Drupal.ajax['view_tracking'], response, status);

                //Выводит  инфу, если нет/некорректный номер отправления
                if (jQuery("#tracking_result div").is("#data_marker")) {
                    jQuery('#data_marker').modal('show');
                }

            };
        }
    };


    //проверка класса
    function checkHasClass(el, sub_el, cl, child_or_parrent) {
        $(el).on('click', sub_el, function (e) {
            // console.log(el);

            if (child_or_parrent == true) {
                if ($(this).hasClass(cl)) {
                    $(this).removeClass(cl);
                } else {
                    $(this).addClass(cl);
                }
            } else if (child_or_parrent == false) {
                if ($(this).parent().hasClass(cl)) {
                    $(this).parent().removeClass(cl);
                } else {
                    $(this).parent().addClass(cl);
                }
            }

            e.preventDefault();
        });
    }

    $(document).ready(function() {
        checkHasClass('#traking_help', '', 'active', true);
        checkHasClass('.block-system', '[id^="track_send_"] > .sender_info', 'show_movie', false);
        checkHasClass('.block-system', '[id^="track_send_"] .from_to_city', 'b-radis', true);
        checkHasClass('.block-system', '[id^="track_send_"] .from_to_city_rf', 'b-radis', true);

        $('.block-system').on('click', '[id^="track_send_"] > .sender_info', function (e) {
            if ($(this).parent().hasClass('show_movie') && $(this).parent().hasClass('refresh')) {

                var tracking_number = $(this).find('.geter_shki').text().substr(0, 14);
                $.ajax({
                    type: "GET", //Метод отправки
                    url: "tracking/get_movie",
                    data: "tracking_number=" + tracking_number,
                    success: function (response) {
                        $('.block-system #track_send_'+tracking_number+' .movie_info').html(response);
                    }
                });
            }

            e.preventDefault();
        });

        $('.block-system').on('click', '[id^="track_send_"] > .del_tracking', function (e) {
            var tracking_number = $(this).parent().attr('id').substr(11);

            $.ajax({
                type: "POST", //Метод отправки
                url: "tracking/delete_tracking",
                data: {
                    tracking_number: tracking_number,
                },
                success: function (response) {
                    $('#tracking_result').html(response);
                }
            });

            e.preventDefault();
        });

        //События для архива
        $('.block-system').on('click', '#tracking_archive_show', function (e) {

            $.ajax({
                type: "POST", //Метод отправки
                url: "tracking/get_archive",
                success: function (response) {
                    $('#tracking_archive').html(response + '<a id="tracking_archive_hide" class="tr_archive"><i class="fas fa-times-circle"></i> Скрыть архив</a>');
                }
            });

            e.preventDefault();
        });
        $('.block-system').on('click', '#tracking_archive_hide', function (e) {
            $('#tracking_archive').html('<a id="tracking_archive_show" class="tr_archive"><i class="fas fa-mail-bulk"></i> Показать архив</a>');


            e.preventDefault();
        });

        //тригер для input ввода
        $('#edit-tracking-group-search-tracking').on("keypress",function(e) {
            var el = $("#view_tracking:not(.progress-disabled)");
            if(e.which == 13 && el) {
                var press = jQuery.Event("keypress");
                press.ctrlKey = false;
                press.which = 13;
                el.trigger(press);

                e.preventDefault();
            }
        });

        jQuery('[data-toggle="help"]').popover({
            trigger: 'hover'
        });

    });


    // var vis = (function(){
    //     var stateKey, eventKey, keys = {
    //         hidden: "visibilitychange",
    //         webkitHidden: "webkitvisibilitychange",
    //         mozHidden: "mozvisibilitychange",
    //         msHidden: "msvisibilitychange"
    //     };
    //     for (stateKey in keys) {
    //         if (stateKey in document) {
    //             eventKey = keys[stateKey];
    //             break;
    //         }
    //     }
    //     return function(c) {
    //         if (c) document.addEventListener(eventKey, c);
    //         return !document[stateKey];
    //     };
    // })();
    //
    // vis(function(){
    //     document.title = vis() ? $("html").load(location.href) : 'Not visible';
    // });
})(jQuery);
