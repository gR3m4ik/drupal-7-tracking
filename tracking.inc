<?php

class Tracking
{
    private $uid;
    private $session_id;

    public function getUserID($session_id)
    {
        global $user;
        if (isset($user->uid) && !empty($user->uid)) {
            $this->uid = $user->uid;
            $this->session_id = $session_id;
        } else {
            $this->uid = 0;
            $this->session_id = $session_id;
        }
    }

    public function showUserID(){
        return $this->uid;
    }

    public function deleteHistory($tracking_number, $id_no_del = '')
    {
        db_set_active();

        if ($this->uid != 0) {
            db_delete('tracking_history')
                ->condition('tracking_number', $tracking_number, '=')
                ->condition('uid', $this->uid, '=')
                ->condition('id', $id_no_del, '!=')
                ->execute();
        } else {
            db_delete('tracking_history')
                ->condition('tracking_number', $tracking_number, '=')
                ->condition('session_id', $this->session_id, '=')
                ->condition('id', $id_no_del, '!=')
                ->execute();
        }
    }

    public function addHistory($tracking_number, $ibci, $status, $ico, $country)
    {
        db_set_active();

        db_insert('tracking_history')->fields(
            array(
                'uid' => $this->uid,
                'session_id' => $this->session_id,
                'ibci' => $ibci,
                'tracking_number' => $tracking_number,
                'status' => $status,
                'date_request' => date('d.m.Y H:i'),
                'ico' => $ico,
                'country' => $country,
            ))->execute();
    }

    function upHistory($tracking_number, $ibci, $status, $ico, $country)
    {
        db_set_active();

        if ($this->uid != 0) {
            db_update('tracking_history')->fields(
                array(
                    'ibci' => $ibci,
                    'session_id' => $this->session_id,
                    'status' => $status,
                    'date_request' => date('d.m.Y H:i'),
                    'ico' => $ico,
                    'country' => $country,
                ))
                ->condition('uid', $this->uid)
                ->condition('tracking_number', $tracking_number)
                ->execute();
        } else {
            db_update('tracking_history')->fields(
                array(
                    'ibci' => $ibci,
                    'status' => $status,
                    'date_request' => date('d.m.Y H:i'),
                    'ico' => $ico,
                    'country' => $country,
                ))
                ->condition('session_id', $this->session_id)
                ->condition('tracking_number', $tracking_number)
                ->execute();
        }


    }

    public function getHistory($tracking_number, $track_date_search = '')
    {
        db_set_active();

        if ($this->uid != 0) {
            $res = db_select('tracking_history', 'n')
                ->fields('n')
                ->condition('n.uid', $this->uid)
                ->condition('n.tracking_number', $tracking_number)
                ->execute()
                ->fetchAll();
        } else {
            $res = db_select('tracking_history', 'n')
                ->fields('n')
                ->condition('n.session_id', $this->session_id)
                ->condition('n.tracking_number', $tracking_number)
                ->execute()
                ->fetchAll();
        }

        return $res;
    }

    public function getHistoryAll()
    {
        if ($this->uid != 0) {
            $res = db_select('tracking_history', 'n')
                ->fields('n')
                ->condition('n.uid', $this->uid)
                ->orderBy("STR_TO_DATE(n.date_request, '%d.%m.%Y %H:%i')", 'DESC')
                ->execute()
                ->fetchAll();
        } else {
//            $res = db_select('tracking_history', 'n')
//                ->fields('n')
//                ->condition('n.session_id', $this->session_id)
//                ->orderBy("STR_TO_DATE(n.date_request, '%d.%m.%Y %H:%i')", 'DESC')
//                ->execute()
//                ->fetchAll();
        }

        return $res;
    }

    public function getSessionCaptcha()
    {
        db_set_active();

        if ($this->uid != 0) {
            $res = db_select('captcha_sessions', 'n')
                ->fields('n')
                ->condition('n.uid', $this->uid)
                ->orderBy('n.csid', 'DESC')
                ->execute()
                ->fetchObject();
        } else {
            $res = db_select('captcha_sessions', 'n')
                ->fields('n')
                ->condition('n.sid', $this->session_id)
                ->orderBy('n.csid', 'DESC')
                ->execute()
                ->fetchObject();
        }

        return $res;
    }

}